var nums = [null, null]
var operator = null;
var opActive = false;
var screen = document.getElementById('op');
var writing = false;

function writeNum(num) {
    if (!writing) {
        writing = true;
    }
    if (screen.innerText == '0' || screen.innerText == String(nums[0]) || screen.innerText == 'ERROR(DIV/0)') {
        screen.innerText = num
    } else {
        screen.innerText += num
    }
}

function activateOp(op) {
    if (!opActive) {
        opActive = true;
        nums[0] = Number(screen.innerText);
        writing = false;
    } else {
        if (writing) {
            nums[1] = Number(screen.innerText);
            makeOp();
            opActive = true;
        }
    }
    operator = op;

}

function makeOp() {
    if (opActive) {
        opActive = false;
        if (writing) {
            nums[1] = Number(screen.innerText);
        }
        n1 = nums[0]
        n2 = nums[1]
        result = 0
        if (operator == '+') {
            result = n1 + n2;
        } else if (operator == '-') {
            result = n1 - n2;
        } else if (operator == 'x') {
            result = n1 * n2;
        } else {
            if (n2 == 0) {
                result = 'ERROR(DIV/0)';
                nums = [null, null]
            } else {
                result = n1 / n2;
            }
        }
        operator = null;
        nums = [result, null];
        screen.innerText = String(nums[0]);
    }

}

function clearAll() {
    nums = [null, null]
    operator = null;
    screen.innerText = '0'
    opActive = false;
    writing = false;
}